import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.StringTokenizer;

import net.sf.json.JSONObject;
import net.sf.json.xml.XMLSerializer;
import edu.stanford.nlp.tagger.maxent.MaxentTagger;
import edu.stanford.nlp.tagger.maxent.TaggerConfig;

public class Tagger_IRNLP {
	public static void main(String[] args) throws IOException,
			ClassNotFoundException {

		String model = "taggers/english-left3words-distsim.tagger";
		TaggerConfig config = new TaggerConfig("-outputFormat", "xml",
				"-model", model);

		MaxentTagger tagger = new MaxentTagger(model, config);
		String raw = TxtReader.readFile("raw\\input.txt");

		// The tagged string
		String tagged = tagger.tagString(raw);

		// String tagged2 = tagger.apply(in)
		// Output the result
//		System.out.println("result:" + tagged);
		
//		String tagged_well= "<?xml version=\"1.0\"?>"+ "\n"+ tagged;  

		// xml to json
/*		String tagged_well= "<data>"+ "\n"+ tagged+"\n</data>";  
		XMLSerializer xmlSerializer = new XMLSerializer();  
//		JSONObject jsonObject1 = (JSONObject) xmlSerializer.read(tagged_well); 

		System.out.println("json result:" + xmlSerializer.read(tagged_well)); */
		// System.out.println(tagged2);
		writeFile(tagged, "data\\output.txt");
		// stringToXML(tagged,"data\\output.xml");
	}

	public static void writeFile(String outputString, String outputFileName) {
		try {

			BufferedWriter out = new BufferedWriter(new FileWriter(
					outputFileName));
			out.write(outputString);
			out.newLine();
			out.close();

		} catch (IOException e) {
			System.err.println(e);
			System.exit(1);
		}
	}

	public static void stringToXML(String input, String fileName)
			throws IOException {
		HashMap<Integer, String[]> map = stringToMap(input);
		JDOMXMLWriter.writeFileUsingJDOM(map, fileName);

	}

	public static HashMap<Integer, String[]> stringToMap(String input) {
		String[] token1 = deli(input, " ");
		HashMap<Integer, String[]> map = new HashMap<Integer, String[]>();

		for (int i = 0; i < token1.length; i++) {
			map.put(i, deli(token1[i], "_"));
		}

		return map;
	}

	public static String[] deli(String f_name, String de) {
		StringTokenizer st = new StringTokenizer(f_name, de);
		String arr[] = new String[st.countTokens()];
		int i = 0;
		while (st.hasMoreTokens()) {
			arr[i] = st.nextToken();
			i++;
		}
		return arr;

	}

}
