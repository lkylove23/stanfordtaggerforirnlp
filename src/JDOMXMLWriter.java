import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

public class JDOMXMLWriter {

	public static void main(String[] args) throws IOException {
		HashMap<Integer, String[]> map = new HashMap<Integer, String[]>();
        String fileName = "data\\output.xml";
        
        String[] S = new String[2];
        S[0]="boy";
        S[1]="NN";
        map.put(0,S);
        
        writeFileUsingJDOM(map, fileName);
        
        System.out.println(map);
    }

	public static void writeFileUsingJDOM(HashMap<Integer, String[]> map,
			String fileName) throws IOException {
		Document doc = new Document();
		doc.setRootElement(new Element("Body"));
//		doc.getRootElement().addContent(new Element("S"));
		
		
		////
		Element Sentence = new Element("S");
		Element CurrentSentence=Sentence;
		doc.getRootElement().addContent(Sentence);
		

		for (int i = 0; i < map.size(); i++) {
			Element Word = new Element("W");
//			System.out.println(map.get(i)[0]);
			if(map.get(i)[1].equals(".")){
				Element Sentence_sep = new Element("S");
				CurrentSentence=Sentence_sep;
				doc.getRootElement().addContent(Sentence_sep);
			}
			Word.setAttribute("c", map.get(i)[1]).setText(map.get(i)[0]);
			CurrentSentence.addContent(Word);
		}
		// JDOM document is ready now, lets write it to file now
		XMLOutputter xmlOutputter = new XMLOutputter(Format.getPrettyFormat());
		// output xml to console for debugging
		// xmlOutputter.output(doc, System.out);
		xmlOutputter.output(doc, new FileOutputStream(fileName));
	}

}